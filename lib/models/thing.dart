import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Thing extends Equatable {
  final String id;
  final String title;
  final String description;
  final String photo;
  final int score;
  final int date;

  Thing({
    this.id,
    @required this.title,
    this.description = "",
    @required this.photo,
    this.score = 0,
    @required this.date
  });

  Thing copyWith({@required String photo}) {
    return Thing(
      id: this.id,
      title: this.title,
      description: this.description,
      photo: photo,
      score: this.score,
      date: this.date,
    );
  }

  Map<String, dynamic> toMap(){
    return <String, dynamic>{
      'title': title,
      'description': description,
      'photo': photo,
      'score': score,
      'date': date,
    };
  }

  Thing.fromMap(Map<String, dynamic> map,String id)
      : assert(map['title'] != null),
        assert(map['photo'] != null),
        id = id,
        title = map['title'],
        description = map['description'],
        photo = map['photo'],
        score = map['score'],
        date = map['date'];

  Thing.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data,snapshot.documentID);

  @override
  List<Object> get props => [id,title, description, photo, score,date];
}
