import 'package:flutter/material.dart';

class Keys {
  static final thingsLoading = const Key('__thingsLoading__');
  static final thingsReload = const Key('__thingsReload__');
  static final thingsOrderedList = const Key('__thingsOrderedList__');

  static var thingItem = const Key('__thingItem__');
  static var thingItemTitle = const Key('__thingItemTitle__');

  static var thingAddScreen = const Key('__thingAddScreen__');

  static var authLoginScreen = const Key('__authLoginScreen__');
}
