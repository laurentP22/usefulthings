import 'package:flutter/material.dart';
class FadeRoute extends PageRouteBuilder {
  final Widget child;
  final RouteSettings routeSettings;
  FadeRoute({this.child,this.routeSettings})
      : super(
    settings: routeSettings,
    pageBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        ) =>
    child,
    transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
        ) =>
        FadeTransition(
          opacity: animation,
          child: child,
        ),
  );
}