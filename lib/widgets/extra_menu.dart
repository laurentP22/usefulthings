import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/screens/auth_login_screen.dart';

class ExtraMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return PopupMenuButton<String>(
        tooltip: "Login / logout user",
        onSelected: (action) {
          if (action == "Login") {
            Navigator.pushNamed(context, AuthLoginScreen.routeName);
          } else {
            BlocProvider.of<AuthBloc>(context).add(AuthDisconnected());
          }
        },
        itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
          state == AuthLogout()
              ? PopupMenuItem<String>(
                  value: "Login",
                  child: Text("Login",),
                )
              : PopupMenuItem<String>(
                  value: "Logout",
                  child: Text("Logout",),
                ),
        ],
        icon: Icon(Icons.more_vert),
      );
    });
  }
}
