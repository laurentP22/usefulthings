import 'package:flutter/material.dart';
import 'package:usefulthings/keys.dart';
import 'package:usefulthings/models/thing.dart';
import 'package:usefulthings/screens/screens.dart';

class ThingItem extends StatelessWidget {
  final Thing thing;
  ThingItem(this.thing);
  @override
  Widget build(BuildContext context) {
    return Padding(
      key: Keys.thingItem,
      padding: EdgeInsets.only(left: 10,right: 10,top: 10),
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            Navigator.pushNamed(context, ThingDetailScreen.routeName,arguments: thing);
          },
          child: ListTile(
            title: Text(thing.title,key: Keys.thingItemTitle,),
            subtitle: Text(thing.description),
            leading: CircleAvatar(
              backgroundImage: thing.photo == null ? AssetImage("assets/icons/ic_placeholder.png"): NetworkImage(thing.photo), // no matter how big it is, it won't overflow
            ),
          )
        ),
      ),
    );
  }
}
