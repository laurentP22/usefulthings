import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/keys.dart';
import 'package:usefulthings/widgets/thing_item.dart';

class OrderedThings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderedThingsBloc, OrderedThingsState>(
      key: Keys.thingsOrderedList,
      builder: (context, state) {
        if (state is OrderedThingsLoadSuccess) {
          final things = state.orderedThings;
          if (things != null && things.length > 0) {
            return ListView.builder(
              itemCount: things.length,
              itemBuilder: (BuildContext context, int index) {
                final thing = things[index];
                return ThingItem(thing,);
              },
            );
          }
          return Center(child: Text("No things"));
        }else{
          return Container();
        }
      },
    );
  }
}
