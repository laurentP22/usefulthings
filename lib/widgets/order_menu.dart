import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/models/order.dart';

class OrderMenu extends StatelessWidget {

@override
Widget build(BuildContext context) {
  final defaultStyle = Theme.of(context).textTheme.body1;
  final activeStyle = Theme.of(context).textTheme.body1.copyWith(color: Theme.of(context).accentColor);
  return BlocBuilder<OrderedThingsBloc, OrderedThingsState>(
      builder: (context, state) {
        return _Menu(
          onSelected: (order) {
            BlocProvider.of<OrderedThingsBloc>(context).add(OrderUpdated(order));
          },
          activeOrder: state is OrderedThingsLoadSuccess ? state.activeOrder : Order.newest,
          activeStyle: activeStyle,
          defaultStyle: defaultStyle,
        );
      });
}
}

class _Menu extends StatelessWidget {
  const _Menu({
    Key key,
    @required this.onSelected,
    @required this.activeOrder,
    @required this.activeStyle,
    @required this.defaultStyle,
  }) : super(key: key);

  final PopupMenuItemSelected<Order> onSelected;
  final Order activeOrder;
  final TextStyle activeStyle;
  final TextStyle defaultStyle;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<Order>(
      tooltip: "Filter to order the list",
      onSelected: onSelected,
      itemBuilder: (BuildContext context) => <PopupMenuItem<Order>>[
        PopupMenuItem<Order>(
          value: Order.newest,
          child: Text(
            "Newest",
            style: activeOrder == Order.newest ? activeStyle : defaultStyle,
          ),
        ),
        PopupMenuItem<Order>(
          value: Order.highest,
          child: Text(
            "Highest",
            style: activeOrder == Order.highest ? activeStyle : defaultStyle,
          ),
        ),
      ],
      icon: Icon(Icons.filter_list),
    );
  }
}
