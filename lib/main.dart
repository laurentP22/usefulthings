import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:usefulthings/screens/screens.dart';
import 'package:usefulthings/widgets/route_pager.dart';

import 'lozalizations.dart';
import 'blocs/blocs.dart';
import 'repository/things_repository.dart';

void main() {
  // BlocSupervisor oversees Blocs and delegates to BlocDelegate.
  // We can set the BlocSupervisor's delegate to an instance of `SimpleBlocDelegate`.
  // This will allow us to handle all transitions and errors in SimpleBlocDelegate.
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) => AuthBloc()
              ..add(
                AuthInitialized(),
              )),
        BlocProvider(
          create: (context) => ThingsBloc(
            thingsRepository: ThingsRepository(),
            authBloc: BlocProvider.of<AuthBloc>(context),
          )..add(ThingsLoaded()),
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'), // English
        const Locale('es'), // Spanish
      ],
      onGenerateTitle: (BuildContext context) =>
          AppLocalizations.of(context).title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: (settings) {
        if (settings.name == ThingDetailScreen.routeName)
          return FadeRoute(child: ThingDetailScreen(),routeSettings: settings);
        else if (settings.name == ThingAddScreen.routeName)
          return FadeRoute(child: ThingAddScreen(),routeSettings: settings);
        else if (settings.name == CameraScreen.routeName)
          return FadeRoute(child: CameraScreen(),routeSettings: settings);
        else if (settings.name == AuthLoginScreen.routeName)
          return FadeRoute(child: AuthLoginScreen(),routeSettings: settings);
        else
          return FadeRoute(
              child: MultiBlocProvider(
            providers: [
              BlocProvider<OrderedThingsBloc>(
                create: (context) => OrderedThingsBloc(
                  thingsBloc: BlocProvider.of<ThingsBloc>(context),
                ),
              ),
            ],
            child: HomeScreen(),
          ));
      },
    );
  }
}