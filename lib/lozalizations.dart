import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppLocalizations {
  final Locale locale;
  bool isTest;

  AppLocalizations(this.locale, {
    this.isTest = false,
  });

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = AppLocalizationsDelegate();
  static const LocalizationsDelegate<AppLocalizations> delegateTest = AppLocalizationsDelegate(isTest: true);

  static Map<String, String> _localizedValues;

  Future<AppLocalizations> loadTest(Locale locale) async {
    return AppLocalizations(locale);
  }

  Future<void> load() async{
    String jsonString = await rootBundle.loadString('assets/lang/${locale.languageCode}.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedValues = jsonMap.map((key,value) => MapEntry(key,value.toString()));
  }

  String get title {
    if (isTest) return 'title';
    return _localizedValues['app_title'];
  }

  String get(String key){
    if (isTest) return key;
    return _localizedValues[key];
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  final bool isTest;

  const AppLocalizationsDelegate({
    this.isTest = false,
  });

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) async{
    AppLocalizations localizations = AppLocalizations(locale,isTest: isTest);
    if (isTest) {
      await localizations.loadTest(locale);
    } else {
      await localizations.load();
    }
    return localizations;
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}