import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

/// Log the user with Google
Future<AuthResult> googleSignIn() async {
  try {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return _auth.signInWithCredential(credential);
  } catch (error) {
    return error;
  }
}

/// Log out the user with Google
Future<String> signOut() async {
  try {
    await _googleSignIn.signOut();
    await _auth.signOut();
    return 'SignOut';
  } catch (e) {
    return e.toString();
  }
}
