import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:usefulthings/models/models.dart';
import 'package:usefulthings/utils/Utils.dart';

class ThingsRepository {
  Future<List<Thing>> loadThings() async {
    var snapshots = await Firestore.instance
        .collection("things")
        .getDocuments(source: Source.server);
    return snapshots.documents.map((doc) => Thing.fromSnapshot(doc)).toList();
  }

  Future<void> addThing(Thing thing) async {
    var target = await tempPhotoPath;
    // compress file and get file.
    var compressFile = await FlutterImageCompress.compressAndGetFile(
      thing.photo,
      target,
      quality: 15,
      rotate: 0,
    );
    final photo = await uploadThingPhoto(compressFile, "${DateTime.now().millisecondsSinceEpoch}");
    var newThing = thing.copyWith(photo: photo);
    Firestore.instance.collection("things").add(newThing.toMap());
  }

  Future<String> uploadThingPhoto(File file, String filename) async {
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child("images/things/$filename");
    final StorageUploadTask uploadTask = storageReference.putFile(file);
    final StorageTaskSnapshot downloadUrl = await uploadTask.onComplete;
    return await downloadUrl.ref.getDownloadURL();
  }

  Future<void> incrementScore(String id) async {
    Firestore.instance
        .collection("things")
        .document(id)
        .updateData({'score': FieldValue.increment(1)});
  }

  Future<void> decrementScore(String id) async {
    Firestore.instance
        .collection("things")
        .document(id)
        .updateData({'score': FieldValue.increment(-1)});
  }
}
