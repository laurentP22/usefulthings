import 'package:flutter/material.dart';
import 'package:fluttercamerax/fluttercamerax.dart';
import 'package:usefulthings/models/models.dart';

class CameraScreen extends StatefulWidget {
  static final String routeName = "/camera_screen";

  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  FlutterCameraXController controller;
  bool flash = false;
  Thing thing;

  void _onFlutterCameraXCreated(FlutterCameraXController controller){
    this.controller = controller;
  }
  void _setFlash(){
    setState(() {
      flash = !flash;
    });
    controller.setFlash(flash);
  }

  void _imageCaptured(String path){
    Navigator.of(context).pop(path);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Camera"),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.black,
            child: FlutterCameraX(
              onFlutterCameraXCreated: _onFlutterCameraXCreated,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: IconButton(
                icon: Icon(Icons.camera, color: Colors.white, size: 50,),
                onPressed: () => controller.capture().then((path) => _imageCaptured(path)),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              margin: EdgeInsets.all(10),
              child: IconButton(
                icon: Icon( flash ? Icons.flash_on : Icons.flash_off, color: Colors.white, size: 30),
                onPressed: _setFlash,
              ),
            ),
          )
        ],
      ),
    );
  }
}
