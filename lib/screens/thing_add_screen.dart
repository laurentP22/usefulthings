import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:usefulthings/keys.dart';
import 'package:usefulthings/lozalizations.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/models/models.dart';
import 'package:usefulthings/repository/repository.dart';
import 'package:usefulthings/screens/camera_screen.dart';

class ThingAddScreen extends StatefulWidget {
  static final routeName = "/thing_add_screen";

  @override
  _ThingAddScreenState createState() => _ThingAddScreenState();
}

class _ThingAddScreenState extends State<ThingAddScreen> {
  final _titleController = TextEditingController();
  final _descriptionController = TextEditingController();
  final thingsRepository = ThingsRepository();
  var photo;

  void _saveThing() {
    final thing = Thing(
        title: _titleController.text,
        description: _descriptionController.text,
        photo: photo,
        score: 0,
        date: DateTime.now().toUtc().millisecondsSinceEpoch);

    BlocProvider.of<ThingsBloc>(context).add(
      ThingAdded(thing),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Keys.thingAddScreen,
      appBar: AppBar(
          title: Text(AppLocalizations.of(context).get('add_thing')),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.camera_alt,
              ),
              onPressed: () =>
                  Navigator.pushNamed(context, CameraScreen.routeName)
                      .then((photo) => this.photo = photo),
            )
          ]),
      body: BlocConsumer<ThingsBloc, ThingsState>(
        listener: (ctx, state) {
          if (state is ThingAddSuccess) {
            BlocProvider.of<ThingsBloc>(context).add(
              ThingsLoaded(),
            );
            Navigator.pop(context);
          } else if (state is ThingAddFailure) {
            final snackBar = SnackBar(content: Text(state.message));
            Scaffold.of(ctx).showSnackBar(snackBar);
          }
        },
        builder: (ctx, state) => Center(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: state == ThingsLoadInProgress()
                ? Center(child: CircularProgressIndicator())
                : SingleChildScrollView(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: 300,
                            child: photo == null
                                ? Image.asset(
                                    "assets/icons/ic_placeholder.png",
                                    fit: BoxFit.none,
                                  )
                                : Image.file(
                                    File(photo),
                                    fit: BoxFit.cover,
                                  ),
                          ),
                          TextField(
                            decoration: InputDecoration(
                                labelText: AppLocalizations.of(context)
                                    .get('label_title')),
                            controller: _titleController,
                          ),
                          TextField(
                            decoration: InputDecoration(
                                labelText: AppLocalizations.of(context)
                                    .get('label_description')),
                            controller: _descriptionController,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child: RaisedButton.icon(
                              onPressed: _saveThing,
                              icon: Icon(Icons.add),
                              label: Text(AppLocalizations.of(context)
                                  .get('add_thing')),
                              elevation: 0,
                              color: Theme.of(context).primaryColor,
                              textColor: Colors.white,
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                            ),
                          ),
                        ]),
                  ),
          ),
        ),
      ),
    );
  }
}
