import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/keys.dart';

class AuthLoginScreen extends StatelessWidget {
  static final routeName = "/auth_login_screen";

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state == AuthLogin()) {
            Navigator.pop(context,true);
          }
        },
        builder: (context, state) {
          return Scaffold(
            key: Keys.authLoginScreen,
            appBar: AppBar(title: Text("Authentication")),
            body: Container(
              child: state == AuthLogout() ? Center(
                child: MaterialButton(
                  onPressed: () => BlocProvider.of<AuthBloc>(context).add(AuthConnected()),
                  color: Colors.white,
                  textColor: Colors.black,
                  child: Text('Login with Google'),
                ),
              ) : Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }
    );
  }
}
