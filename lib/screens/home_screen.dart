import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/blocs/things/things_bloc.dart';
import 'package:usefulthings/keys.dart';
import 'package:usefulthings/widgets/extra_menu.dart';
import 'package:usefulthings/widgets/order_menu.dart';
import 'package:usefulthings/widgets/ordered_things.dart';

import '../lozalizations.dart';
import 'screens.dart';

class HomeScreen extends StatelessWidget {
  static final routeName = "/";

  void retryLoad(BuildContext context) {
    BlocProvider.of<ThingsBloc>(context).add(ThingsLoaded());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).title),
          actions: [
            OrderMenu(),
            ExtraMenu(),
          ],
        ),
        body: BlocBuilder<ThingsBloc, ThingsState>(
          builder: (context, state) {
            if (state == ThingsLoadInProgress()) {
              return Center(child: CircularProgressIndicator(key: Keys.thingsLoading));
            } else if (state == ThingsLoadFailure()) {
              return Center(
                  key: Keys.thingsReload,
                  child: RaisedButton(
                child: Text('Retry'),
                onPressed: () => retryLoad(context),
              ));
            } else {
              return OrderedThings();
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          heroTag: "btnAdd",
          onPressed: () {
            if (BlocProvider.of<AuthBloc>(context).state == AuthLogin()) {
              Navigator.pushNamed(context, ThingAddScreen.routeName);
            } else {
              Navigator.pushNamed(context, AuthLoginScreen.routeName).then((connected) {
                if (connected != null && connected) {
                  Navigator.pushNamed(context, ThingAddScreen.routeName);
                }
              });
            }
          },
          child: Icon(Icons.add),
          tooltip: "Add thing",
        ));
  }
}
