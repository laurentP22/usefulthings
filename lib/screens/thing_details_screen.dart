import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/models/models.dart';
import 'package:usefulthings/screens/auth_login_screen.dart';

class ThingDetailScreen extends StatelessWidget {
  static final routeName = "/thing_details_screen";

  @override
  Widget build(BuildContext context) {
    final thing = ModalRoute.of(context).settings.arguments as Thing;

    void increment(BuildContext context) {
      if (BlocProvider.of<AuthBloc>(context).state == AuthLogin()) {
        BlocProvider.of<ThingsBloc>(context).add(
          ThingIncremented(thing.id),
        );
      } else {
        Navigator.pushNamed(context, AuthLoginScreen.routeName)
            .then((connected) {
          if (connected != null && connected) {
            BlocProvider.of<ThingsBloc>(context).add(
              ThingIncremented(thing.id),
            );
          }
        });
      }
    }

    void decrement(BuildContext context) {
      if (BlocProvider.of<AuthBloc>(context).state == AuthLogin()) {
        BlocProvider.of<ThingsBloc>(context).add(
          ThingDecremented(thing.id),
        );
      } else {
        Navigator.pushNamed(context, AuthLoginScreen.routeName)
            .then((connected) {
          if (connected != null && connected) {
            BlocProvider.of<ThingsBloc>(context).add(
              ThingDecremented(thing.id),
            );
          }
        });
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('Thing detail'),
        ),
        body: BlocConsumer<ThingsBloc, ThingsState>(
          listener: (ctx, state) {
            if (state is ThingUpdateSuccess) {
              BlocProvider.of<ThingsBloc>(context).add(ThingsLoaded(),);
              Navigator.pop(context);
            } else if (state is ThingUpdateFailure) {
              final snackBar = SnackBar(content: Text(state.message));
              Scaffold.of(ctx).showSnackBar(snackBar);
            }
          },
          builder: (ctx, state) => state == ThingsLoadInProgress()
              ? Center(child: CircularProgressIndicator())
              : Center(
                  child: Column(
                    children: [
                      Container(
                        width: double.infinity,
                        height: 300,
                        child: thing.photo == null
                            ? Image.asset(
                                "assets/icons/ic_placeholder.png",
                                fit: BoxFit.none,
                              )
                            : Image.network(
                                thing.photo,
                                fit: BoxFit.cover,
                              ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(
                          thing.title,
                          style: Theme.of(context).textTheme.headline,
                        ),
                      ),
                      Text(
                        thing.description,
                        style: Theme.of(context).textTheme.subhead,
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Text(
                            "Score:${thing.score}",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ),
        ),
        floatingActionButton: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
              child: FloatingActionButton(
                  heroTag: "btnIncrement",
                  child: Icon(Icons.add),
                  onPressed: () => increment(context)),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
              child: FloatingActionButton(
                  heroTag: "btnDecrement",
                  child: Icon(Icons.remove),
                  onPressed: () => decrement(context)),
            ),
          ],
        ));
  }
}
