import 'package:path_provider/path_provider.dart';

Future<String> get tempPhotoPath async {
  final directory = await getTemporaryDirectory();
  return "${directory.path}/${DateTime.now().millisecondsSinceEpoch}.jpg";
}