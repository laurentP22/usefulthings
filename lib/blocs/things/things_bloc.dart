import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:usefulthings/models/models.dart';
import 'package:usefulthings/repository/repository.dart';

import './things.dart';
import '../blocs.dart';

class ThingsBloc extends Bloc<ThingsEvent, ThingsState> {
  final ThingsRepository thingsRepository;
  final AuthBloc authBloc;

  ThingsBloc({@required this.thingsRepository, @required this.authBloc});

  @override
  ThingsState get initialState => ThingsLoadInProgress();

  @override
  Stream<ThingsState> mapEventToState(ThingsEvent event) async* {
    if (event is ThingsLoaded) {
      yield* _mapThingsLoadedToState(event);
    } else if (event is ThingAdded) {
      yield* _mapThingsAddedToState(event);
    } else if (event is ThingIncremented) {
      yield* _mapThingIncreasedToState(event);
    } else if (event is ThingDecremented) {
      yield* _mapThingDecreasedToState(event);
    }
  }

  Stream<ThingsState> _mapThingsLoadedToState(ThingsLoaded event) async* {
    yield ThingsLoadInProgress();
    try {
      final things = await thingsRepository.loadThings();
      yield ThingsLoadSuccess(things,);
    } catch (_) {
      yield ThingsLoadFailure();
    }
  }

  Stream<ThingsState> _mapThingsAddedToState(ThingAdded event) async* {
    yield ThingsLoadInProgress();

    if(authBloc.state != AuthLogin()){
      yield ThingAddFailure(message: "User not connected");
      return;
    }

    try {
        await _addThing(event.thing);
        yield ThingAddSuccess();
    } catch (_) {
      yield ThingAddFailure();
    }
  }

  Stream<ThingsState> _mapThingIncreasedToState(ThingIncremented event) async* {
    yield ThingsLoadInProgress();

    if(authBloc.state != AuthLogin()){
      yield ThingUpdateFailure(message: "User not connected");
      return;
    }

    try {
      await _incrementScore(event.id);
      yield ThingUpdateSuccess();
    } catch (_) {
      yield ThingUpdateFailure();
    }
  }

  Stream<ThingsState> _mapThingDecreasedToState(ThingDecremented event) async* {
    yield ThingsLoadInProgress();

    if(authBloc.state != AuthLogin()){
      yield ThingUpdateFailure(message: "User not connected");
      return;
    }

    try {
      await _decrementScore(event.id);

      yield ThingUpdateSuccess();
    } catch (error) {
      yield ThingUpdateFailure(message: error);
    }
  }

  Future<void> _addThing(Thing thing) {
    return thingsRepository.addThing(thing);
  }

  Future<void> _incrementScore(String id) {
    return thingsRepository.incrementScore(id);
  }

  Future<void> _decrementScore(String id) {
    return thingsRepository.decrementScore(id);
  }
}
