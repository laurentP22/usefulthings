import 'package:equatable/equatable.dart';
import 'package:usefulthings/models/models.dart';

abstract class ThingsState extends Equatable {
  const ThingsState();

  @override
  List<Object> get props => [];
}

class ThingsLoadInProgress extends ThingsState {}

class ThingsLoadSuccess extends ThingsState {
  final List<Thing> things;

  const ThingsLoadSuccess([this.things = const []]);

  @override
  List<Object> get props => [things];

  @override
  String toString() => 'ThingsLoadSuccess { things: ${things.map((thing)=>thing.title)}}';
}

class ThingsLoadFailure extends ThingsState {}

class ThingAddSuccess extends ThingsState {}

class ThingAddFailure extends ThingsState {
  final String message;
  ThingAddFailure({this.message = ""});

  @override
  List<Object> get props => [message];
}

class ThingUpdateSuccess extends ThingsState {}

class ThingUpdateFailure extends ThingsState {
  final String message;
  ThingUpdateFailure({this.message = ""});

  @override
  List<Object> get props => [message];
}