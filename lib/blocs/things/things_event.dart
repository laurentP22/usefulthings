import 'package:equatable/equatable.dart';
import 'package:usefulthings/models/thing.dart';


abstract class ThingsEvent extends Equatable {
  const ThingsEvent();

  @override
  List<Object> get props => [];
}

class ThingsLoaded extends ThingsEvent {}

class ThingAdded extends ThingsEvent {
  final Thing thing;

  const ThingAdded(this.thing);

  @override
  List<Object> get props => [thing];

  @override
  String toString() => 'ThingAdded';
}

class ThingIncremented extends ThingsEvent {
  final String id;

  const ThingIncremented(this.id);

  @override
  List<Object> get props => [id];

  @override
  String toString() => 'ThingIncremented';
}

class ThingDecremented extends ThingsEvent {
  final String id;

  const ThingDecremented(this.id);

  @override
  List<Object> get props => [id];

  @override
  String toString() => 'ThingDecremented';
}
