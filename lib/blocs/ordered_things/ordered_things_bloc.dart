import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/models/models.dart';

import './ordered_things.dart';

class OrderedThingsBloc extends Bloc<OrderedThingsEvent, OrderedThingsState> {
  final ThingsBloc thingsBloc;
  StreamSubscription thingsSubscription;

  OrderedThingsBloc({@required this.thingsBloc}) {
    thingsSubscription = thingsBloc.listen((state) {
      if (state is ThingsLoadSuccess) {
        add(OrderedThingsUpdated((thingsBloc.state as ThingsLoadSuccess).things));
      }
    });
  }

  @override
  OrderedThingsState get initialState {
    return OrderedThingsLoadInProgress();
  }

  @override
  Stream<OrderedThingsState> mapEventToState(OrderedThingsEvent event) async* {
     if (event is OrderUpdated) {
      yield* _mapOrderUpdatedToState(event);
    } else if (event is OrderedThingsUpdated) {
      yield* _mapThingsUpdatedToState(event);
    }
  }

  Stream<OrderedThingsState> _mapOrderUpdatedToState(OrderUpdated event,) async* {
    if (thingsBloc.state is ThingsLoadSuccess) {
      yield OrderedThingsLoadSuccess(
        _mapThingsToOrderThings((thingsBloc.state as ThingsLoadSuccess).things, event.order,),
        event.order,
      );
    }
  }

  Stream<OrderedThingsState> _mapThingsUpdatedToState(OrderedThingsUpdated event,) async* {
    final order = state is OrderedThingsLoadSuccess ? (state as OrderedThingsLoadSuccess).activeOrder : Order.newest;
    yield OrderedThingsLoadSuccess(
      _mapThingsToOrderThings((thingsBloc.state as ThingsLoadSuccess).things, order,),
      order,
    );
  }

  List<Thing> _mapThingsToOrderThings(List<Thing> things, Order order) {
    if(things == null || things.length == 0){
      return [];
    }
    if(order == Order.newest){
       things.sort((a,b) => b.date.compareTo(a.date));
       return things;
    }else if( order == Order.highest){
      things.sort((a,b) => b.score.compareTo(a.score));
      return things;
    }else{
      return things;
    }
  }

  @override
  Future<void> close() {
    thingsSubscription?.cancel();
    return super.close();
  }
}
