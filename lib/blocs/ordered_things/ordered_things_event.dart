import 'package:equatable/equatable.dart';
import 'package:usefulthings/models/order.dart';
import 'package:usefulthings/models/thing.dart';

abstract class OrderedThingsEvent extends Equatable {
  const OrderedThingsEvent();

  @override
  List<Object> get props => [];
}

class OrderUpdated extends OrderedThingsEvent {
  final Order order;

  const OrderUpdated(this.order);

  @override
  List<Object> get props => [order];

  @override
  String toString() => 'OrderUpdated { order: $order }';
}

class OrderedThingsUpdated extends OrderedThingsEvent {
  final List<Thing> things;

  const OrderedThingsUpdated(this.things);

  @override
  List<Object> get props => [things];

  @override
  String toString() => 'OrderedThingsUpdated { things: ${things.map((thing)=>thing.title)} }';
}

