import 'package:equatable/equatable.dart';
import 'package:usefulthings/models/models.dart';

abstract class OrderedThingsState extends Equatable {
  const OrderedThingsState();

  @override
  List<Object> get props => [];
}

class OrderedThingsLoadInProgress extends OrderedThingsState {}

class OrderedThingsLoadSuccess extends OrderedThingsState {
  final List<Thing> orderedThings;
  final Order activeOrder;

  const OrderedThingsLoadSuccess(this.orderedThings, this.activeOrder,);

  @override
  List<Object> get props => [orderedThings, activeOrder];

  @override
  String toString() {
    return 'OrderedThingsLoadSuccess { orderedThings: ${orderedThings.map((thing)=>thing.title)}, activeOrder: $activeOrder }';
  }
}
