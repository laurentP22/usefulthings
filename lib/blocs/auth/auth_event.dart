import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}
class AuthInitialized extends AuthEvent {}

class AuthConnected extends AuthEvent {}

class AuthDisconnected extends AuthEvent {}
