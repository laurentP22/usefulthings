import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  const AuthState();
  @override
  List<Object> get props => [];
}

class AuthInProgress extends AuthState {}

class AuthInitialState extends AuthState {}

class AuthLogin extends AuthState {}

class AuthLogout extends AuthState {}
