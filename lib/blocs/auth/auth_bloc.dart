import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:usefulthings/auth.dart';

import './auth.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  AuthState get initialState => AuthInitialState();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthInitialized) {
      yield* _mapAuthInitializedToState();
    } else if (event is AuthConnected) {
      yield* _mapAuthConnectedToState();
    } else if (event is AuthDisconnected) {
      yield* _mapAuthDisconnectedAddedToState(event);
    }
  }

  Stream<AuthState> _mapAuthInitializedToState() async* {
    var user = await _auth.currentUser();
    if (user != null) {
      yield AuthLogin();
    } else {
      yield AuthLogout();
    }
  }

  Stream<AuthState> _mapAuthConnectedToState() async* {
    yield AuthInProgress();
    try{
      AuthResult authResult = await googleSignIn();
      if (authResult.user != null) {
        yield AuthLogin();
      } else {
        yield AuthLogout();
      }
    }catch(error){
      yield AuthLogout();
    }
  }

  Stream<AuthState> _mapAuthDisconnectedAddedToState(AuthDisconnected event) async* {
    signOut();
    yield AuthLogout();
  }
}
