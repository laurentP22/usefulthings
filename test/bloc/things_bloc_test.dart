import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/models/models.dart';
import 'package:usefulthings/repository/repository.dart';

class MockThingsRepository extends Mock implements ThingsRepository {}
class MockAuthBloc extends MockBloc<AuthEvent, AuthState>
    implements AuthBloc {}

void main() {
  group('TodosBloc', () {
    ThingsRepository thingsRepository;
    ThingsBloc thingsBloc;
    AuthBloc authBloc;

    final thing = Thing(
        id: "abc",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 5,
        date: 12121212);

    setUp(() {
      thingsRepository = MockThingsRepository();
      authBloc = MockAuthBloc();
      when(thingsRepository.loadThings()).thenAnswer((_)=> Future.value([thing]));
      when(authBloc.state).thenReturn(AuthLogin(),);
      thingsBloc = ThingsBloc(thingsRepository: thingsRepository, authBloc: authBloc);
    });

    blocTest<ThingsBloc, ThingsEvent, ThingsState>(
      'should emit ThingsLoadSuccess when receiving event ThingsLoaded',
      build: () async => thingsBloc,
      act: (ThingsBloc bloc) async => bloc.add(ThingsLoaded()),
      expect: <ThingsState>[
        ThingsLoadSuccess([thing]),
      ],
    );

    blocTest<ThingsBloc, ThingsEvent, ThingsState>(
      'should emit ThingsLoadFailure when event ThingsLoaded throw error',
      build: () async {
        when(thingsRepository.loadThings()).thenThrow((_)=> "No connection");
        return thingsBloc;
      },
      act: (ThingsBloc bloc) async => bloc.add(ThingsLoaded()),
      expect: <ThingsState>[
        ThingsLoadFailure(),
      ],
    );

    blocTest<ThingsBloc, ThingsEvent, ThingsState>(
      'should emit ThingAddSuccess when receiving event ThingAdded (user login)',
      build: () async => thingsBloc,
      act: (ThingsBloc bloc) async =>bloc..add(ThingsLoaded())..add(ThingAdded(thing)),
      expect: <ThingsState>[
        ThingsLoadSuccess([thing]),
        ThingsLoadInProgress(),
        ThingAddSuccess(),
      ],
    );

    blocTest<ThingsBloc, ThingsEvent, ThingsState>(
      'should emit ThingAddFailure when receiving event ThingAdded',
      build: () async {
        when(thingsRepository.addThing(thing)).thenThrow((_)=> Future.value("error"));
        return thingsBloc;
      },
      act: (ThingsBloc bloc) async =>bloc..add(ThingsLoaded())..add(ThingAdded(thing)),
      expect: <ThingsState>[
        ThingsLoadSuccess([thing]),
        ThingsLoadInProgress(),
        ThingAddFailure(),
      ],
    );

    blocTest<ThingsBloc, ThingsEvent, ThingsState>(
      'should emit ThingAddFailure when receiving event ThingAdded (user logout)',
      build: () async {
        when(authBloc.state).thenReturn(AuthLogout(),);
        return thingsBloc;
      },
      act: (ThingsBloc bloc) async =>bloc..add(ThingsLoaded())..add(ThingAdded(thing)),
      expect: <ThingsState>[
        ThingsLoadSuccess([thing]),
        ThingsLoadInProgress(),
        ThingAddFailure(message: "User not connected")
      ],
    );
  });
}
