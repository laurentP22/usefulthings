import 'dart:async';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/models/models.dart';
import 'package:usefulthings/repository/repository.dart';

class MockThingsBloc extends MockBloc<ThingsEvent, ThingsState>
    implements ThingsBloc {}

class MockThingsRepository extends Mock implements ThingsRepository {}

void main() {
  MockThingsBloc mockThingsBloc;
  final thing = Thing(
      id: "abc",
      title: "Title",
      description: "Description",
      photo: "path/to/photo",
      score: 5,
      date: 12121212);

  final things = [
    Thing(
        id: "a1",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 3,
        date: 11),
    Thing(
        id: "a3",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 4,
        date: 33),
    Thing(
        id: "a2",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 5,
        date: 22)
  ];

  final newestThings = [
    Thing(
        id: "a3",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 4,
        date: 33),
    Thing(
        id: "a2",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 5,
        date: 22),
    Thing(
        id: "a1",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 3,
        date: 11),
  ];
  final highestThings = [
    Thing(
        id: "a2",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 5,
        date: 22),
    Thing(
        id: "a3",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 4,
        date: 33),
    Thing(
        id: "a1",
        title: "Title",
        description: "Description",
        photo: "path/to/photo",
        score: 3,
        date: 11),
  ];
  setUp(() {
    mockThingsBloc = MockThingsBloc();
  });

  group('OrderedThingsBloc', () {
    test('Update the things list must update the ordered list', () async {
      final mockThingsBloc = MockThingsBloc();
      when(mockThingsBloc.state).thenReturn(
        ThingsLoadSuccess([thing]),
      );
      whenListen(
        mockThingsBloc,
        Stream<ThingsState>.fromIterable([
          ThingsLoadSuccess([thing]),
        ]),
      );
      await expectLater(
          OrderedThingsBloc(thingsBloc: mockThingsBloc),
          emitsInOrder([
            OrderedThingsLoadInProgress(),
            OrderedThingsLoadSuccess(
              [thing],
              Order.newest,
            ),
          ]));
    });

    blocTest<OrderedThingsBloc, OrderedThingsEvent, OrderedThingsState>(
      'should order by score (highest)',
      build: () async {
        when(mockThingsBloc.state).thenReturn(ThingsLoadSuccess(things));
        return OrderedThingsBloc(thingsBloc: mockThingsBloc);
      },
      act: (bloc) async => bloc..add(OrderUpdated(Order.highest)),
      expect: [
        OrderedThingsLoadSuccess(highestThings, Order.highest),
      ],
    );

    blocTest<OrderedThingsBloc, OrderedThingsEvent, OrderedThingsState>(
      'should order by date (newest)',
      build: () async {
        when(mockThingsBloc.state).thenReturn(ThingsLoadSuccess(things));
        return OrderedThingsBloc(thingsBloc: mockThingsBloc);
      },
      act: (bloc) async => bloc..add(OrderUpdated(Order.newest)),
      expect: [
        OrderedThingsLoadSuccess(newestThings, Order.newest),
      ],
    );
  });
}
