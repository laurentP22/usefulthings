import 'dart:io';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/keys.dart';
import 'package:usefulthings/models/models.dart';
import 'package:usefulthings/widgets/ordered_things.dart';

class MockAuthBloc extends MockBloc<AuthEvent, AuthState> implements AuthBloc {}

class MockThingsBloc extends MockBloc<ThingsEvent, ThingsState>
    implements ThingsBloc {}

class MockFilteredThingsBloc
    extends MockBloc<OrderedThingsBloc, OrderedThingsState>
    implements OrderedThingsBloc {}

void main() {
  group('Things', () {
    AuthBloc authBloc;
    ThingsBloc thingsBloc;
    OrderedThingsBloc orderedThingsBloc;

    final things = [
      Thing(
          id: "a1",
          title: "Title",
          description: "Description",
          photo:
              "https://firebasestorage.googleapis.com/v0/b/usefulthings-cd034.appspot.com/o/images%2Fthings%2F1587680749782?alt=media&token=979c94ad-2429-48de-b4dd-3d8a7b89e17f",
          score: 1,
          date: 11),
      Thing(
          id: "a2",
          title: "Title",
          description: "Description",
          photo:
              "https://firebasestorage.googleapis.com/v0/b/usefulthings-cd034.appspot.com/o/images%2Fthings%2F1587680749782?alt=media&token=979c94ad-2429-48de-b4dd-3d8a7b89e17f",
          score: 5,
          date: 11),
    ];

    setUp(() {
      authBloc = MockAuthBloc();
      thingsBloc = MockThingsBloc();
      orderedThingsBloc = MockFilteredThingsBloc();
      HttpOverrides.global = null;
    });

    testWidgets('should show a list of 2 items when state is ThingsLoadSuccess',
        (WidgetTester tester) async {
      when(orderedThingsBloc.state).thenAnswer(
        (_) => OrderedThingsLoadSuccess(things, Order.newest),
      );
      await tester.pumpWidget(
        MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<ThingsBloc>.value(value: thingsBloc),
            BlocProvider<OrderedThingsBloc>.value(value: orderedThingsBloc),
          ],
          child: MaterialApp(
            home: Scaffold(
              body: OrderedThings(),
            ),
          ),
        ),
      );
      await tester.pump(Duration(seconds: 1));
      expect(find.byKey(Keys.thingsOrderedList), findsOneWidget);
      expect(find.byKey(Keys.thingItem), findsNWidgets(2));
    });
  });
}
