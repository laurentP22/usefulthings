import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:usefulthings/blocs/blocs.dart';
import 'package:usefulthings/keys.dart';
import 'package:usefulthings/lozalizations.dart';
import 'package:usefulthings/main.dart';
import 'package:usefulthings/screens/screens.dart';

class MockAuthBloc extends MockBloc<AuthEvent, AuthState> implements AuthBloc {}

class MockThingsBloc extends MockBloc<ThingsEvent, ThingsState>
    implements ThingsBloc {}

class MockFilteredThingsBloc
    extends MockBloc<OrderedThingsBloc, OrderedThingsState>
    implements OrderedThingsBloc {}

Widget makeTestableWidget({Widget child}) {
  return MediaQuery(
    data: MediaQueryData(),
    child: MaterialApp(
        localizationsDelegates: [
          AppLocalizations.delegateTest,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        home: child,
        routes: {
          ThingDetailScreen.routeName: (ctx) => ThingDetailScreen(),
          ThingAddScreen.routeName: (ctx) => ThingAddScreen(),
          CameraScreen.routeName: (ctx) => CameraScreen(),
          AuthLoginScreen.routeName: (ctx) => AuthLoginScreen(),
        }),
  );
}

void main() {
  // REf : https://medium.com/flutterpub/testing-localized-widget-in-flutter-3bfa5492bc84
  group('Things', () {
    AuthBloc authBloc;
    ThingsBloc thingsBloc;
    OrderedThingsBloc orderedThingsBloc;

    setUp(() {
      authBloc = MockAuthBloc();
      thingsBloc = MockThingsBloc();
      orderedThingsBloc = MockFilteredThingsBloc();
    });

    testWidgets('should show loading indicator when state is ThingsLoading',
        (WidgetTester tester) async {
      when(thingsBloc.state).thenAnswer(
        (_) => ThingsLoadInProgress(),
      );

      await tester.pumpWidget(
        MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<ThingsBloc>.value(value: thingsBloc),
            BlocProvider<OrderedThingsBloc>.value(value: orderedThingsBloc),
          ],
          child: makeTestableWidget(child: HomeScreen()),
        ),
      );
      await tester.pump(Duration(seconds: 1));
      expect(find.byKey(Keys.thingsLoading), findsOneWidget);
    });

    testWidgets(
        'should show OrderedThings widget when state is ThingsLoadSuccess',
        (WidgetTester tester) async {
      when(thingsBloc.state).thenAnswer(
        (_) => ThingsLoadSuccess(),
      );

      await tester.pumpWidget(
        MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<ThingsBloc>.value(value: thingsBloc),
            BlocProvider<OrderedThingsBloc>.value(value: orderedThingsBloc),
          ],
          child: makeTestableWidget(child: HomeScreen()),
        ),
      );
      await tester.pumpAndSettle();

      expect(find.byKey(Keys.thingsLoading), findsNothing);
      expect(find.byKey(Keys.thingsOrderedList), findsOneWidget);
    });

    testWidgets(
        'should show RaisedButton widget when state is ThingsLoadFailure',
        (WidgetTester tester) async {
      when(thingsBloc.state).thenAnswer(
        (_) => ThingsLoadFailure(),
      );

      await tester.pumpWidget(
        MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<ThingsBloc>.value(value: thingsBloc),
            BlocProvider<OrderedThingsBloc>.value(value: orderedThingsBloc),
          ],
          child: makeTestableWidget(child: HomeScreen()),
        ),
      );
      await tester.pumpAndSettle();

      expect(find.byKey(Keys.thingsLoading), findsNothing);
      expect(find.byKey(Keys.thingsReload), findsOneWidget);
    });

    testWidgets(
        'should show Thing_Add_Screen widget after pressing add button (login)',
        (WidgetTester tester) async {
      when(thingsBloc.state).thenAnswer(
        (_) => ThingsLoadSuccess(),
      );
      when(authBloc.state).thenAnswer(
        (_) => AuthLogin(),
      );

      await tester.pumpWidget(
        MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<ThingsBloc>.value(value: thingsBloc),
            BlocProvider<OrderedThingsBloc>.value(value: orderedThingsBloc),
          ],
          child: MyApp(),
        ),
      );
      await tester.pumpAndSettle();

      await tester.tap(find.byIcon(Icons.add));
      await tester.pumpAndSettle();

      expect(find.byKey(Keys.thingAddScreen), findsOneWidget);
      expect(find.byKey(Keys.authLoginScreen), findsNothing);
    });

    testWidgets(
        'should show Auth_Login_Screen widget after pressing add button (logout)',
        (WidgetTester tester) async {
      when(thingsBloc.state).thenAnswer(
        (_) => ThingsLoadSuccess(),
      );
      when(authBloc.state).thenAnswer(
        (_) => AuthLogout(),
      );

      await tester.pumpWidget(
        MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<ThingsBloc>.value(value: thingsBloc),
            BlocProvider<OrderedThingsBloc>.value(value: orderedThingsBloc),
          ],
          child: makeTestableWidget(child: HomeScreen()),
        ),
      );
      await tester.pumpAndSettle();

      await tester.tap(find.byIcon(Icons.add));
      await tester.pumpAndSettle();

      expect(find.byKey(Keys.thingAddScreen), findsNothing);
      expect(find.byKey(Keys.authLoginScreen), findsOneWidget);
    });
  });
}
